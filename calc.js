"use strict";
import BinaryNode from "./BinaryNode.js";
{
  let lastResult = 0;
  let inputBox;
  let resultBox;
  let expression = null;
  const MAX_CHARACTERS = 12;
  const DOT_SIGN = ".";
  const PLUS_SIGN = "+";
  const MINUS_SIGN = "-";
  const DIVISION_SIGN = "/";
  const MULTIPLY_SIGN = "*";
  const EQUAL_SIGN = "=";
  const CLEAN_SIGN = "c";
  const DEFAULT_TEXT_SIZE = "30px";
  const ENTER_SIGN = "Enter";
  const OPERATIONS = {
    [PLUS_SIGN]: (firstOperand, secondOperand) => firstOperand + secondOperand,
    [MINUS_SIGN]: (firstOperand, secondOperand) => firstOperand - secondOperand,
    [MULTIPLY_SIGN]: (firstOperand, secondOperand) =>
      firstOperand * secondOperand,
    [DIVISION_SIGN]: (firstOperand, secondOperand) =>
      firstOperand / secondOperand,
  };
  const ADD_EXPRESSION = {
    [PLUS_SIGN]: (operand) =>
      expression.addSeconderyExpression(PLUS_SIGN, operand),
    [MINUS_SIGN]: (operand) =>
      expression.addSeconderyExpression(MINUS_SIGN, operand),
    [MULTIPLY_SIGN]: (operand) =>
      expression.addPrimaryExpression(MULTIPLY_SIGN, operand),
    [DIVISION_SIGN]: (operand) =>
      expression.addPrimaryExpression(DIVISION_SIGN, operand),
  };
  const NON_NUMERIC_VALID_CHARACTERS = {
    [PLUS_SIGN]: () => operatorHandle(PLUS_SIGN),
    [MULTIPLY_SIGN]: () => operatorHandle(MULTIPLY_SIGN),
    [DIVISION_SIGN]: () => operatorHandle(DIVISION_SIGN),
    [MINUS_SIGN]: () => handleMinusOperator(),
    [EQUAL_SIGN]: () => handleResultRequest(),
    [DOT_SIGN]: () => handleNewDot(),
    [CLEAN_SIGN]: () => cleanAllCalculatorValues(),
    [ENTER_SIGN]: () => handleResultRequest(),
  };

  window.onload = () => {
    const CLICK_EVENT = "click";
    registerCalculatorBoxesEventListener();
    registerDigitsEventListener(CLICK_EVENT);
    registerOperatorsEventListener(CLICK_EVENT);
    registerDeletesEventListener(CLICK_EVENT);
    document
      .getElementById("result")
      .addEventListener(CLICK_EVENT, handleResultRequest);
    document
      .getElementById("minus")
      .addEventListener(CLICK_EVENT, handleMinusOperator);
  };

  const registerCalculatorBoxesEventListener = () => {
    inputBox = document.getElementById("input-box");
    inputBox.addEventListener("keypress", handleInputChange);
    inputBox.focus();
    inputBox.addEventListener("blur", () => inputBox.focus());
    resultBox = document.getElementById("result-box");
  };

  const registerDigitsEventListener = (CLICK_EVENT) => {
    const digits = document.getElementsByClassName("digit");

    for (let digit of digits) {
      digit.addEventListener(CLICK_EVENT, handleNewEnteredDigit);
    }

    document.getElementById("dot").addEventListener(CLICK_EVENT, handleNewDot);
  };

  const registerOperatorsEventListener = (CLICK_EVENT) => {
    const operators = document.getElementsByClassName("operator");

    for (let operator of operators) {
      operator.addEventListener(CLICK_EVENT, handleNewOperatorClick);
    }
  };

  const registerDeletesEventListener = (CLICK_EVENT) => {
    document
      .getElementById("delete")
      .addEventListener(CLICK_EVENT, deleteLastCharacter);
    document
      .getElementById("delete-all")
      .addEventListener(CLICK_EVENT, cleanAllCalculatorValues);
  };

  const handleNewEnteredDigit = (event) => {
    const clickedDigit = event.target.innerText;
    tryToCleanLastExpression();

    if (isOnlyZeroInInputBox()) {
      trimZero();
    }

    if (getInputBoxText().length < MAX_CHARACTERS) {
      addTextToInputBox(clickedDigit);
    }
  };

  const handleNewOperatorClick = (event) => {
    const operator = event.target.innerText;
    operatorHandle(operator);
  };

  const deleteLastCharacter = () => {
    changeInputBoxText(getInputBoxText().slice(0, -1));
  };

  const cleanAllCalculatorValues = () => {
    cleanResultBoxText();
    cleanInputBoxText();
    revertCalculatorParametersToDefault();
  };

  const handleResultRequest = () => {
    if (getResultBoxText().includes(EQUAL_SIGN)) {
      return;
    }

    getInputBoxText() === "" && getResultBoxText() !== ""
      ? cleanOperatorBeforeResult()
      : addOperandToExpression(Number(getInputBoxText()));
    lastResult = Number(
      calculateMathExpression(expression).toPrecision(MAX_CHARACTERS)
    );
    resultBox.innerHTML += `= <br/> ${lastResult}`;
  };

  const handleInputChange = (event) => {
    const character = event.key;

    if (isKeyInvalid(character)) {
      event.preventDefault();
      return;
    }

    isNonNumericValidCharacter(character)
      ? handleNonNumericCharacters(event, character)
      : tryToCleanLastExpression();

    if (isOnlyZeroInInputBox()) {
      trimZero();
    }
  };

  const handleNewDot = () => {
    tryToCleanLastExpression();

    if (getInputBoxText() === "") {
      changeInputBoxText("0.");
    } else if (!getInputBoxText().includes(DOT_SIGN)) {
      addTextToInputBox(DOT_SIGN);
    }
  };

  const handleMinusOperator = () => {
    tryToCleanLastExpression();

    if (getInputBoxText() === "") {
      addTextToInputBox(MINUS_SIGN);
    } else if (getInputBoxText() !== MINUS_SIGN) {
      operatorHandle(MINUS_SIGN);
    }
  };

  const isResultBoxReadyForNewCalculation = () =>
    getResultBoxText().includes(EQUAL_SIGN) || getResultBoxText() === "";

  const newResultBoxText = () => {
    return isResultBoxReadyForNewCalculation()
      ? lastResult
      : getResultBoxText().slice(0, -1);
  };

  const tryToCleanLastExpression = () => {
    if (getResultBoxText().includes(EQUAL_SIGN)) {
      cleanResultBoxText();
      expression = null;
      resultBox.style.fontSize = DEFAULT_TEXT_SIZE;
    }
  };

  const operatorHandle = (operator) => {
    if (isInputBoxWithNumber()) {
      addOperandToExpression(Number(getInputBoxText()));
      addTextToResultBox(operator);
      return;
    }

    if (isResultBoxReadyForNewCalculation()) {
      expression = new BinaryNode(lastResult);
    }

    changeResultBoxText(newResultBoxText() + operator);
    cleanInputBoxText();
  };

  const handleNegativeNumber = () => {
    const operand = Number(getInputBoxText());

    if (getResultBoxText() === "") {
      addTextToResultBox(lastResult);
      expression = new BinaryNode(lastResult);
      ADD_EXPRESSION[PLUS_SIGN](operand);
    } else {
      handleOperatorBeforeNegativeNumber();
      addNegativeOperandToExpression(operand);
    }
  };

  const addNegativeOperandToExpression = (operand) => {
    const lastCharacter = getResultBoxText().slice(-1);
    isMultiplyOrDivisionSign(lastCharacter)
      ? ADD_EXPRESSION[lastCharacter](operand)
      : ADD_EXPRESSION[PLUS_SIGN](operand);
  };

  const calculateMathExpression = (root) => {
    if (!root.hasChild()) {
      return root.data;
    }

    return OPERATIONS[root.data](
      calculateMathExpression(root.left),
      calculateMathExpression(root.right)
    );
  };

  const isKeyInvalid = (character) =>
    /[^0-9.+*\-\/=c]/.test(character) && character !== ENTER_SIGN;

  const isInputBoxWithNumber = () =>
    getInputBoxText() !== "" && getInputBoxText() !== MINUS_SIGN;

  const isMultiplyOrDivisionSign = (character) => /[*\/]/.test(character);

  const addOperandToExpression = (operand) => {
    if (operand < 0) {
      handleNegativeNumber();
    } else {
      expression === null
        ? (expression = new BinaryNode(operand))
        : ADD_EXPRESSION[getResultBoxText().slice(-1)](operand);
    }

    addTextToResultBox(operand);
    cleanInputBoxText();
  };

  const isOnlyZeroInInputBox = () =>
    getInputBoxText() === "0" || getInputBoxText() === "-0";

  const trimZero = () => {
    changeInputBoxText(getInputBoxText().slice(0, -1));
  };

  const cleanOperatorBeforeResult = () => {
    changeResultBoxText(getResultBoxText().slice(0, -1));
  };

  const handleOperatorBeforeNegativeNumber = () => {
    const lastOperator = getResultBoxText().slice(-1);
    if ([PLUS_SIGN, MINUS_SIGN].includes(lastOperator)) {
      changeResultBoxText(getResultBoxText().slice(0, -1));
    }
  };

  const handleNonNumericCharacters = (event, character) => {
    NON_NUMERIC_VALID_CHARACTERS[character]();
    event.preventDefault();
  };

  const isNonNumericValidCharacter = (character) => /[^0-9]/.test(character);

  const getInputBoxText = () => inputBox.value;

  const changeInputBoxText = (newText) => {
    inputBox.value = newText;
  };

  const cleanInputBoxText = () => {
    inputBox.value = "";
  };

  const addTextToInputBox = (newText) => {
    inputBox.value += newText;
  };

  const getResultBoxText = () => resultBox.innerText;

  const cleanResultBoxText = () => {
    resultBox.innerText = "";
  };

  const changeResultBoxText = (newText) => {
    resultBox.innerText = newText;
  };

  const addTextToResultBox = (newText) => {
    resultBox.innerText += newText;
    tryToResizeResultBoxText();
  };

  const tryToResizeResultBoxText = () => {
    if (getResultBoxText().length > MAX_CHARACTERS + 1) {
      resultBox.style.fontSize = String(generateNewTextSize()) + "px";
    }
  };

  const generateNewTextSize = () => {
    const reductionSize = 0.85;

    return (
      DEFAULT_TEXT_SIZE.split("px")[0] *
      Math.pow(
        reductionSize,
        parseInt(getResultBoxText().length / MAX_CHARACTERS)
      )
    );
  };

  const revertCalculatorParametersToDefault = () => {
    lastResult = 0;
    expression = null;
    resultBox.style.fontSize = DEFAULT_TEXT_SIZE;
  };
}
