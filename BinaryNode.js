export default class BinaryNode {
  constructor(data, right = null, left = null) {
    this.data = data;
    this.right = right;
    this.left = left;
  }

  addPrimaryExpression(operator, operand) {
    const lsatExpression = this.getLastExpression();
    const newTree = new BinaryNode(operator);
    newTree.right = new BinaryNode(operand);
    newTree.left = new BinaryNode(
      lsatExpression.data,
      lsatExpression.right,
      lsatExpression.left
    );
    lsatExpression.copyFromObject(newTree);
  }

  addSeconderyExpression(operator, operand) {
    const newTreeRoot = new BinaryNode(operator);
    newTreeRoot.left = new BinaryNode(this.data, this.right, this.left);
    newTreeRoot.right = new BinaryNode(operand);
    this.copyFromObject(newTreeRoot);
  }

  hasChild() {
    return this.left !== null && this.right !== null;
  }

  getLastExpression() {
    let currentNode = this;
    while (currentNode.hasChild() && !["/", "*"].includes(currentNode.data)) {
      currentNode = currentNode.right;
    }

    return currentNode;
  }

  copyFromObject(object) {
    this.data = object.data;
    this.right = object.right;
    this.left = object.left;
  }
}
